package test.java;

import main.java.org.calculator.InterfataGrafica.MonoPolinom.Monom;
import main.java.org.calculator.InterfataGrafica.MonoPolinom.Polinom;
import main.java.org.calculator.InterfataGrafica.MonoPolinom.RegeX;
import main.java.org.calculator.InterfataGrafica.Operations.Inmultire;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class InmultireTest {

    @Test
    void opInmultireTest() {
        RegeX ax = new RegeX("3x^4+2x^2-4x+1");
        RegeX bx = new RegeX("3x^6+7x^4+5x^2+6x-5");

        Polinom a = ax.generatePolinom(new Polinom(0,new HashMap<Integer, Monom>()));
        Polinom b = bx.generatePolinom(new Polinom(0,new HashMap<Integer,Monom>()));

        Inmultire mul = new Inmultire(a,b);
        Polinom c = mul.op();
        Polinom d = new Polinom(0,new HashMap<Integer,Monom>());
        c.completeTheMissingValuesWith0();

        d.addMonom(new Monom(9,10));
        d.addMonom(new Monom(27,8));
        d.addMonom(new Monom(-12,7));
        d.addMonom(new Monom(32,6));
        d.addMonom(new Monom(-10,5));
        d.addMonom(new Monom(2,4));
        d.addMonom(new Monom(-8,3));
        d.addMonom(new Monom(-29,2));
        d.addMonom(new Monom(26,1));
        d.addMonom(new Monom(-5,0));
        d.completeTheMissingValuesWith0();

        assertEquals(d.getExpresie().get(0).getCoeficient(),c.getExpresie().get(0).getCoeficient());
        assertEquals(d.getExpresie().get(1).getCoeficient(),c.getExpresie().get(1).getCoeficient());
        assertEquals(d.getExpresie().get(2).getCoeficient(),c.getExpresie().get(2).getCoeficient());
        assertEquals(d.getExpresie().get(3).getCoeficient(),c.getExpresie().get(3).getCoeficient());
        assertEquals(d.getExpresie().get(4).getCoeficient(),c.getExpresie().get(4).getCoeficient());
        assertEquals(d.getExpresie().get(5).getCoeficient(),c.getExpresie().get(5).getCoeficient());
        assertEquals(d.getExpresie().get(6).getCoeficient(),c.getExpresie().get(6).getCoeficient());
        assertEquals(d.getExpresie().get(7).getCoeficient(),c.getExpresie().get(7).getCoeficient());
        assertEquals(d.getExpresie().get(8).getCoeficient(),c.getExpresie().get(8).getCoeficient());
        assertEquals(d.getExpresie().get(9).getCoeficient(),c.getExpresie().get(9).getCoeficient());
        assertEquals(d.getExpresie().get(10).getCoeficient(),c.getExpresie().get(10).getCoeficient());
    }
}