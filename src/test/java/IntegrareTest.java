package test.java;

import main.java.org.calculator.InterfataGrafica.MonoPolinom.Monom;
import main.java.org.calculator.InterfataGrafica.MonoPolinom.Polinom;
import main.java.org.calculator.InterfataGrafica.MonoPolinom.RegeX;
import main.java.org.calculator.InterfataGrafica.Operations.Integrare;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class IntegrareTest {

    @Test
    void opIntegrareTest() {
        RegeX ax = new RegeX("3x^4+2x^2-4x+1");
        Polinom a = ax.generatePolinom(new Polinom(0,new HashMap<Integer, Monom>()));

        Integrare integrare = new Integrare(a);
        Polinom c = integrare.op();
        c.completeTheMissingValuesWith0();

        Polinom d = new Polinom(0,new HashMap<Integer,Monom>());

        d.addMonom(new Monom((3.0/5.0),5));
        d.addMonom(new Monom(2.0/3.0,3));
        d.addMonom(new Monom(-2,2));
        d.addMonom(new Monom(1,1));
        d.completeTheMissingValuesWith0();

        assertEquals(d.getExpresie().get(0).getCoeficient(),c.getExpresie().get(0).getCoeficient());
        assertEquals(d.getExpresie().get(1).getCoeficient(),c.getExpresie().get(1).getCoeficient());
        assertEquals(d.getExpresie().get(2).getCoeficient(),c.getExpresie().get(2).getCoeficient());
        assertEquals(d.getExpresie().get(3).getCoeficient(),c.getExpresie().get(3).getCoeficient());
        assertEquals(d.getExpresie().get(4).getCoeficient(),c.getExpresie().get(4).getCoeficient());
        assertEquals(d.getExpresie().get(5).getCoeficient(),c.getExpresie().get(5).getCoeficient());
    }
}