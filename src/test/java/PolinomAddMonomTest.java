package test.java;

import main.java.org.calculator.InterfataGrafica.MonoPolinom.Monom;
import main.java.org.calculator.InterfataGrafica.MonoPolinom.Polinom;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

public class PolinomAddMonomTest {
    @Test
    public void addMonomInEmptyPolinomCheckGrad(){
        var polinom = new Polinom(0,new HashMap<Integer, Monom>());
        var monom = new Monom(4.0,1);
        polinom.addMonom(monom);
        assertEquals(1,polinom.getGrad());
    }
    @Test
    public void addMonomInEmptyPolinomCheckCoeficent(){
        var polinom = new Polinom(0,new HashMap<Integer,Monom>());
        var monom = new Monom(4.0,1);
        polinom.addMonom(monom);
        assertEquals(4, polinom.getExpresie().get(1).getCoeficient());
    }
    @Test
    public void addMonom3InEmptyPolinomCheckGrad(){
        var polinom = new Polinom(0,new HashMap<Integer,Monom>());
        var monom = new Monom(4.0,3);
        polinom.addMonom(monom);
        assertEquals(3,polinom.getGrad());
    }
    @Test
    public void addMonom3InEmptyPolinomCheckCoeficent(){
        var polinom = new Polinom(0,new HashMap<Integer,Monom>());
        var monom = new Monom(4.0,3);
        polinom.addMonom(monom);
        assertEquals(4, polinom.getExpresie().get(3).getCoeficient());
    }
    @Test
    public void addMonomInNormalPolinomCheck(){
        var termeni = new HashMap<Integer,Monom>();
        termeni.put(3,new Monom(4,3));
        termeni.put(2,new Monom(3,2));
        termeni.put(0,new Monom(4,0));
        var polinom = new Polinom(3,termeni);
        var monom = new Monom(4,5);
        polinom.addMonom(monom);
        assertEquals(5,polinom.getGrad());
        assertEquals(4,polinom.getExpresie().get(5).getCoeficient());
    }
    @Test
    public void addMonomOnOccupiedPositionCheck(){
        var termeni = new HashMap<Integer,Monom>();
        termeni.put(3,new Monom(4,3));
        termeni.put(2,new Monom(3,2));
        termeni.put(0,new Monom(4,0));
        var polinom = new Polinom(3,termeni);
        var monom = new Monom(4,3);
        polinom.addMonom(monom);
        assertEquals(3,polinom.getGrad());
        assertEquals(8,polinom.getExpresie().get(3).getCoeficient());
    }
}