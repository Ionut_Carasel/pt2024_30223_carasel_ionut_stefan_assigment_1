package test.java;

import main.java.org.calculator.InterfataGrafica.MonoPolinom.Monom;
import main.java.org.calculator.InterfataGrafica.MonoPolinom.Polinom;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

public class PolinomGradUpdateTest {
    @Test
    public void gradUpdateCheck0(){
        var polinom = new Polinom(5,new HashMap<Integer, Monom>());
        polinom.completeTheMissingValuesWith0();
        polinom.gradUpdate();
        assertEquals(0,polinom.getGrad());
    }
    @Test
    public void gradUpdateCheck1(){
        var polinom = new Polinom(5,new HashMap<Integer,Monom>());
        polinom.addMonom(new Monom(5.0,3));
        polinom.completeTheMissingValuesWith0();
        polinom.gradUpdate();
        assertEquals(3,polinom.getGrad());
    }
}