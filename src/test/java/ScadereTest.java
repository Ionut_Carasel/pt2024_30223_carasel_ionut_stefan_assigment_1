package test.java;

import main.java.org.calculator.InterfataGrafica.MonoPolinom.Monom;
import main.java.org.calculator.InterfataGrafica.MonoPolinom.Polinom;
import main.java.org.calculator.InterfataGrafica.MonoPolinom.RegeX;
import main.java.org.calculator.InterfataGrafica.Operations.Scadere;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class ScadereTest {

    @Test
    void opScadereTest() {
        RegeX ax = new RegeX("6*x+3x^6-5+7*x^4+5x^2");
        RegeX bx = new RegeX("3x^4+2x^2-4x+1");

        Polinom a = ax.generatePolinom(new Polinom(0,new HashMap<Integer, Monom>()));
        Polinom b = bx.generatePolinom(new Polinom(0,new HashMap<Integer,Monom>()));

        Scadere sub = new Scadere(a,b);
        Polinom c = sub.op();
        Polinom d = new Polinom(0,new HashMap<Integer,Monom>());

        c.completeTheMissingValuesWith0();
        c.gradUpdate();

        d.addMonom(new Monom(3,6));
        d.addMonom(new Monom(4,4));
        d.addMonom(new Monom(3,2));
        d.addMonom(new Monom(10,1));
        d.addMonom(new Monom(-6,0));
        d.completeTheMissingValuesWith0();

        assertEquals(d.getExpresie().get(0).getCoeficient(),c.getExpresie().get(0).getCoeficient());
        assertEquals(d.getExpresie().get(1).getCoeficient(),c.getExpresie().get(1).getCoeficient());
        assertEquals(d.getExpresie().get(2).getCoeficient(),c.getExpresie().get(2).getCoeficient());
        assertEquals(d.getExpresie().get(3).getCoeficient(),c.getExpresie().get(3).getCoeficient());
        assertEquals(d.getExpresie().get(4).getCoeficient(),c.getExpresie().get(4).getCoeficient());
        assertEquals(d.getExpresie().get(5).getCoeficient(),c.getExpresie().get(5).getCoeficient());
        assertEquals(d.getExpresie().get(6).getCoeficient(),c.getExpresie().get(6).getCoeficient());
    }
}