package test.java;

import main.java.org.calculator.InterfataGrafica.MonoPolinom.Monom;
import main.java.org.calculator.InterfataGrafica.MonoPolinom.Polinom;
import main.java.org.calculator.InterfataGrafica.MonoPolinom.RegeX;
import main.java.org.calculator.InterfataGrafica.Operations.Derivare;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class DerivareTest {

    @Test
    void opDerivareTest() {
        RegeX ax = new RegeX("3x^4+2x^2-4x+1");
        Polinom a = ax.generatePolinom(new Polinom(0,new HashMap<Integer, Monom>()));

        Derivare dx = new Derivare(a);
        Polinom c = dx.op();
        c.completeTheMissingValuesWith0();

        Polinom d = new Polinom(0,new HashMap<Integer,Monom>());

        d.addMonom(new Monom(12,3));
        d.addMonom(new Monom(4,1));
        d.addMonom(new Monom(-4,0));
        d.completeTheMissingValuesWith0();

        assertEquals(d.getExpresie().get(0).getCoeficient(),c.getExpresie().get(0).getCoeficient());
        assertEquals(d.getExpresie().get(1).getCoeficient(),c.getExpresie().get(1).getCoeficient());
        assertEquals(d.getExpresie().get(2).getCoeficient(),c.getExpresie().get(2).getCoeficient());
        assertEquals(d.getExpresie().get(3).getCoeficient(),c.getExpresie().get(3).getCoeficient());
    }
}