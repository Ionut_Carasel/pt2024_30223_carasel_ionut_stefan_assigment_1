package test.java;

import main.java.org.calculator.InterfataGrafica.MonoPolinom.Monom;
import main.java.org.calculator.InterfataGrafica.MonoPolinom.Polinom;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class PolinomToStringTest {

    @Test
    void testToString() {
        var polinom = new Polinom(0,new HashMap<Integer, Monom>());
        polinom.addMonom(new Monom(1,3));
        polinom.addMonom(new Monom(2,4));
        polinom.addMonom(new Monom(1,1));
        polinom.addMonom(new Monom(9,0));
        polinom.completeTheMissingValuesWith0();
        assertEquals("2*x^4 + x^3 + x + 9",polinom.toString());
    }
}