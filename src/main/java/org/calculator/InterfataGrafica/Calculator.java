package main.java.org.calculator.InterfataGrafica;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import main.java.org.calculator.InterfataGrafica.MonoPolinom.*;

public class Calculator extends JDialog {
    private JTextField textField1;
    private JTextField textField2;
    private JButton a7Button;
    private JButton a8Button;
    private JButton a9Button;
    private JButton a4Button;
    private JButton a5Button;
    private JButton a6Button;
    private JButton a1Button;
    private JButton a0Button;
    private JButton a2Button;
    private JButton a3Button;
    private JButton addButton;
    private JButton subtractButton;
    private JButton derivButton;
    private JButton integralButton;
    private JButton multiplyButton;
    private JButton divideButton;
    private JButton equalsButton;
    private JPanel content;
    private JLabel result;
    private JButton switchButton;
    private JButton xButton;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton CEButton;
    private JLabel op;
    private JButton resetButton;
    private boolean selected2;

    private int opSelected;

    public Calculator(JDialog parent) {
        super(parent);
        selected2 = false;
        setTitle("CaraselCulator");
        setContentPane(content);
        setMinimumSize(new Dimension(500, 500));
        setModal(true);
        setLocationRelativeTo(parent);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        a0Button.addActionListener(e -> {
            if (!selected2) {
                textField1.setText(textField1.getText() + "0");
            } else {
                textField2.setText(textField2.getText() + "0");
            }
        });
        a1Button.addActionListener(e -> {
            if (!selected2) {
                textField1.setText(textField1.getText() + "1");
            } else {
                textField2.setText(textField2.getText() + "1");
            }
        });
        a2Button.addActionListener(e -> {
            if (!selected2) {
                textField1.setText(textField1.getText() + "2");
            } else {
                textField2.setText(textField2.getText() + "2");
            }
        });
        a3Button.addActionListener(e -> {
            if (!selected2) {
                textField1.setText(textField1.getText() + "3");
            } else {
                textField2.setText(textField2.getText() + "3");
            }
        });
        a4Button.addActionListener(e -> {
            if (!selected2) {
                textField1.setText(textField1.getText() + "4");
            } else {
                textField2.setText(textField2.getText() + "4");
            }
        });
        a5Button.addActionListener(e -> {
            if (!selected2) {
                textField1.setText(textField1.getText() + "5");
            } else {
                textField2.setText(textField2.getText() + "5");
            }
        });
        a6Button.addActionListener(e -> {
            if (!selected2) {
                textField1.setText(textField1.getText() + "6");
            } else {
                textField2.setText(textField2.getText() + "6");
            }
        });
        a7Button.addActionListener(e -> {
            if (!selected2) {
                textField1.setText(textField1.getText() + "7");
            } else {
                textField2.setText(textField2.getText() + "7");
            }
        });
        a8Button.addActionListener(e -> {
            if (!selected2) {
                textField1.setText(textField1.getText() + "8");
            } else {
                textField2.setText(textField2.getText() + "8");
            }
        });
        a9Button.addActionListener(e -> {
            if (!selected2) {
                textField1.setText(textField1.getText() + "9");
            } else {
                textField2.setText(textField2.getText() + "9");
            }
        });
        switchButton.addActionListener(e -> {
            selected2 = !selected2;
        });
        xButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!selected2) {
                    textField1.setText(textField1.getText() + "x");
                } else {
                    textField2.setText(textField2.getText() + "x");
                }
            }
        });
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!selected2) {
                    textField1.setText(textField1.getText() + "+");
                } else {
                    textField2.setText(textField2.getText() + "+");
                }
            }
        });
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!selected2) {
                    textField1.setText(textField1.getText() + "-");
                } else {
                    textField2.setText(textField2.getText() + "-");
                }
            }
        });
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!selected2) {
                    textField1.setText(textField1.getText() + "*");
                } else {
                    textField2.setText(textField2.getText() + "*");
                }
            }
        });
        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!selected2) {
                    textField1.setText(textField1.getText() + "^");
                } else {
                    textField2.setText(textField2.getText() + "^");
                }
            }
        });
        CEButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!selected2) {
                    textField1.setText("");
                } else {
                    textField2.setText("");
                }
            }
        });
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                opSelected = 1;
                op.setText("+");
            }
        });
        subtractButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                opSelected = 2;
                op.setText("-");
            }
        });
        multiplyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                opSelected = 3;
                op.setText("*");
            }
        });
        divideButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                opSelected = 4;
                op.setText("/");
            }
        });
        derivButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                opSelected = 5;
                op.setText("d/dx");
            }
        });
        integralButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                opSelected = 6;
                op.setText("∫");
            }
        });
        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                opSelected = 0;
                op.setText("");
            }
        });
        equalsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(opSelected==0){
                    result.setText("Nu ai ales nicio operatie");
                } else if (opSelected>0 && opSelected<5) {
                    Egal egal = new Egal(textField1.getText(),textField2.getText());
                    Polinom rezultat = egal.egal2(opSelected);
                    result.setText(rezultat.toString());
                } else if (opSelected==5) {
                    Egal egal = new Egal(textField1.getText());
                    Polinom rezultat = egal.egal1(opSelected);
                    result.setText(rezultat.toString());
                } else if (opSelected==6) {
                    Egal egal =  new Egal(textField1.getText());
                    Polinom rezultat = egal.egal1(opSelected);
                    result.setText(rezultat.toString()+"+C");
                }
            }
        });

        setVisible(true);
    }
}
