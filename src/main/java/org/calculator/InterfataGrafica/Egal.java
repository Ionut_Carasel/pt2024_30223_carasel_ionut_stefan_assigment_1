package main.java.org.calculator.InterfataGrafica;

import java.util.*;
import main.java.org.calculator.InterfataGrafica.MonoPolinom.*;
import main.java.org.calculator.InterfataGrafica.Operations.*;

public class Egal {
    private String s1;
    private String s2;

    public Egal(String s1) {
        this.s1 = s1;
    }
    public Egal(String s1, String s2) {
        this.s1 = s1;
        this.s2 = s2;
    }

    public String getS1() {
        return s1;
    }

    public void setS1(String s1) {
        this.s1 = s1;
    }

    public String getS2() {
        return s2;
    }

    public void setS2(String s2) {
        this.s2 = s2;
    }

    public Polinom egal2(int opSelected){
        RegeX polinom1Regex = new RegeX(s1);
        RegeX polinom2Regex = new RegeX(s2);

        Polinom a = polinom1Regex.generatePolinom(new Polinom(0,new HashMap<Integer,Monom>()));
        Polinom b = polinom2Regex.generatePolinom(new Polinom(0,new HashMap<Integer,Monom>()));
        return switch (opSelected) {
            case 1 -> {
                Adunare add = new Adunare(a, b);
                yield add.op();
            }
            case 2 -> {
                Scadere sub = new Scadere(a, b);
                yield sub.op();
            }
            case 3 -> {
                Inmultire mul = new Inmultire(a, b);
                yield mul.op();
            }
            case 4 -> {
                Impartire div = new Impartire(a, b);
                yield div.op();
            }
            default -> throw new IllegalStateException("Unexpected value: " + opSelected);
        };
    }
    public Polinom egal1(int opSelected){
        RegeX polinomRegex  =new RegeX(s1);
        Polinom a = polinomRegex.generatePolinom(new Polinom(0,new HashMap<Integer,Monom>()));
        Polinom c = new Polinom(0,new HashMap<Integer,Monom>());

        if(opSelected == 5){
            Derivare dx = new Derivare(a);
            c = dx.op();
        }
        if(opSelected == 6){
            Integrare integrare = new Integrare(a);
            c = integrare.op();
        }
        return c;
    }
}
