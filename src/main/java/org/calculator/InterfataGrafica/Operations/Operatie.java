package main.java.org.calculator.InterfataGrafica.Operations;

import main.java.org.calculator.InterfataGrafica.MonoPolinom.Polinom;
public class Operatie{
    private Polinom a;
    private Polinom b;

    public Operatie(Polinom a, Polinom b) {
        this.a = a;
        this.b = b;
    }

    public Operatie(Polinom a) {
        this.a = a;
    }

    public Polinom getA() {
        return a;
    }

    public void setA(Polinom a) {
        this.a = a;
    }

    public Polinom getB() {
        return b;
    }

    public Polinom op(){
        return new Polinom(0);
    }
}
