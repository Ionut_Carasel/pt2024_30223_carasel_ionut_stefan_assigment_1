package main.java.org.calculator.InterfataGrafica.Operations;

import main.java.org.calculator.InterfataGrafica.MonoPolinom.Monom;
import main.java.org.calculator.InterfataGrafica.MonoPolinom.Polinom;

import java.util.HashMap;

public class Impartire extends Operatie {
    public Impartire(Polinom a, Polinom b) {
        super(a, b);
    }
    @Override
    public Polinom op(){
        int gradC = this.getA().getGrad() - this.getB().getGrad();
        HashMap<Integer, Monom> rezultat = new HashMap<>();

        if(gradC<0){
            // Dacă gradul dividendului este mai mic decât cel al divizorului,
            // rezultatul împărțirii este 0
            rezultat.put(0,new Monom(0));
            return new Polinom(0,rezultat);
        }
        else{
            while(gradC >= 0){
                //realizam impartirea propriu zisa a polinomului
                Monom m = this.getA().getExpresie().get(gradC);
                Monom mDiv = this.getB().getExpresie().get(this.getB().getGrad());

                double coefAux = m.getCoeficient();
                int expAux = m.getExponent() - mDiv.getExponent();
                Monom mRezultat = new Monom(coefAux,expAux);
                rezultat.put(expAux,mRezultat);

                //actualizam polinomul A cu restul
                Polinom rest = new Rest(this.getA(),new Inmultire(this.getA(),this.getB()).op()).op();
                this.setA(rest);

                gradC = this.getA().getGrad() - this.getB().getGrad();
            }
            return new Polinom(gradC,rezultat);
        }
    }
}
