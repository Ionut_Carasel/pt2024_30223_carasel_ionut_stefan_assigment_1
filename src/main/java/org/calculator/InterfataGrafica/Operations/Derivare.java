package main.java.org.calculator.InterfataGrafica.Operations;

import main.java.org.calculator.InterfataGrafica.MonoPolinom.Monom;
import main.java.org.calculator.InterfataGrafica.MonoPolinom.Polinom;

import java.util.HashMap;
public class Derivare extends Operatie {

    public Derivare(Polinom a) {
        super(a);
    }

    @Override
    public Polinom op(){
        if(this.getA().getGrad()==0){
            HashMap<Integer,Monom>termeni = new HashMap<>();
            Polinom c = new Polinom(0,termeni);
            c.getExpresie().put(0,new Monom(0.0,0));
            return c;
        }
        else{
            int gradC = this.getA().getGrad()-1;
            HashMap<Integer, Monom>termeni = new HashMap<>();
            Polinom c = new Polinom(gradC,termeni);

            for(int i = 1; i<= this.getA().getGrad(); i++){
                if(this.getA().getExpresie().containsKey(i)){
                    int newExp = i - 1;
                    double newCoef = this.getA().getExpresie().get(i).getCoeficient()*i;
                    c.getExpresie().put(newExp,new Monom(newCoef,newExp));
                }
            }
            return c;
        }
    }
}
