package main.java.org.calculator.InterfataGrafica.Operations;

import main.java.org.calculator.InterfataGrafica.MonoPolinom.Monom;
import main.java.org.calculator.InterfataGrafica.MonoPolinom.Polinom;

import java.util.*;
public class Adunare extends Operatie {
    public Adunare(Polinom a, Polinom b) {
        super(a, b);
    }
    @Override
    public Polinom op(){
        int gradC = Math.max(getA().getGrad(), getB().getGrad());
        HashMap<Integer, Monom> termeni = new HashMap<>();
        Polinom c = new Polinom(gradC, termeni);

        for(int i = 0; i<=gradC; i++){
            //am declarat 2 variabile auxiliare care sa stocheze valorile coeficientiilor
            double valA;
            double valB;
            //daca exista vreo valoare la cheia i(adica daca exista un termen care il are pe x la puterea i)
            //se memoreaza in variabila valA, iar in caz contrar valA va fi 0
            if(getA().getExpresie().containsKey(i)){
                valA = getA().getExpresie().get(i).getCoeficient();
            }else valA = 0.0;
            //analog
            if(getB().getExpresie().containsKey(i)){
                valB = getB().getExpresie().get(i).getCoeficient();
            }else valB = 0.0;
            //se adauga la noul polinom la map-ul expresiei la cheia i, Monomul care are coeficientul suma variabilelor
            //auxiliare si exponentul i
            c.getExpresie().put(i,new Monom((valA+valB),i));
        }
        return c;
    }
}
