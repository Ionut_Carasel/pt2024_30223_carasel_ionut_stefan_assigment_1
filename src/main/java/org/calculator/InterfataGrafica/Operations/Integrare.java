package main.java.org.calculator.InterfataGrafica.Operations;

import main.java.org.calculator.InterfataGrafica.MonoPolinom.Monom;
import main.java.org.calculator.InterfataGrafica.MonoPolinom.Polinom;

import java.util.HashMap;

public class Integrare extends Operatie {

    public Integrare(Polinom a) {
        super(a);
    }

    @Override
    public Polinom op(){
        int gradC = this.getA().getGrad()+1;
        HashMap<Integer, Monom>termeni = new HashMap<>();
        Polinom c = new Polinom(gradC,termeni);

        for(int i = 0; i<= this.getA().getGrad(); i++){
            if(this.getA().getExpresie().containsKey(i)){
                int newExp = i + 1;
                double newCoef = this.getA().getExpresie().get(i).getCoeficient()/newExp;
                c.getExpresie().put(newExp,new Monom(newCoef,newExp));
            }
        }
        c.getExpresie().put(0,new Monom(0.0,0));
        return c;
    }
}
