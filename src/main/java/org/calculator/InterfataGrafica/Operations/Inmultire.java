package main.java.org.calculator.InterfataGrafica.Operations;

import main.java.org.calculator.InterfataGrafica.MonoPolinom.Monom;
import main.java.org.calculator.InterfataGrafica.MonoPolinom.Polinom;

import java.util.HashMap;
public class Inmultire extends Operatie {
    public Inmultire(Polinom a, Polinom b) {
        super(a, b);
    }

    @Override
    public Polinom op(){
        int gradC = this.getA().getGrad() + this.getB().getGrad();
        HashMap<Integer, Monom>termeni = new HashMap<>();
        Polinom c = new Polinom(gradC, termeni);
        //parcurgem fiecare valoare din map-ul polinomului a
        for(int i = 0; i<= getA().getGrad(); i++){
            //daca exista valoare la cheia i (adica daca avem termen cu coeficient nenul la puterea i)
            //parcurgem si map-ul polinomului b,inmultim fiecare coeficient cu coeficientul lui i si insumam fiecare exponent
            //cu exponentul i, apoi costruim monoame noi pe baza acestor valori si le adaugam in polinomul dat
            if(this.getA().getExpresie().containsKey(i)){
                for(int j = 0; j<= getB().getGrad();j++){
                    if(this.getB().getExpresie().containsKey(j)){
                        double newCoef = this.getA().getExpresie().get(i).getCoeficient()*this.getB().getExpresie().get(j).getCoeficient();
                        int newExp = this.getA().getExpresie().get(i).getExponent()+this.getB().getExpresie().get(j).getExponent();
                        c.addMonom(new Monom(newCoef,newExp));
                    }
                }
            }
        }

        return c;
    }
}
