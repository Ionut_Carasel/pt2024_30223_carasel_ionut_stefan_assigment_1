package main.java.org.calculator.InterfataGrafica.Operations;

import main.java.org.calculator.InterfataGrafica.MonoPolinom.Monom;
import main.java.org.calculator.InterfataGrafica.MonoPolinom.Polinom;

import java.util.HashMap;
public class Rest extends Operatie {
    public Rest(Polinom a, Polinom b) {
        super(a, b);
    }
    @Override
    public Polinom op() {
        HashMap<Integer, Monom> rest = new HashMap<>(this.getA().getExpresie());

        for (int expA : this.getA().getExpresie().keySet()) {
            Monom m = this.getA().getExpresie().get(expA);
            if (this.getB().getExpresie().containsKey(expA)) {
                Monom mDiv = this.getB().getExpresie().get(expA);
                double coef = m.getCoeficient() - mDiv.getCoeficient();
                if (coef != 0) {
                    rest.put(expA, new Monom(coef));
                } else {
                    rest.remove(expA);
                }
            }
        }

        return new Polinom(this.getA().getGrad(), rest);
    }
}

