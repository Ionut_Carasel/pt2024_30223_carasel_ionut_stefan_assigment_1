package main.java.org.calculator.InterfataGrafica.MonoPolinom;

import java.util.*;
public class Polinom {
    private int grad;
    private HashMap<Integer, Monom> expresie;
    public Polinom(int grad, HashMap<Integer,Monom> expresie) {
        this.grad = grad;
        this.expresie = expresie;
    }
    public Polinom(int grad) {
        this.grad = grad;
    }
    public Polinom(HashMap<Integer,Monom> expresie) {
        this.expresie = expresie;
    }

    public int getGrad() {
        return grad;
    }

    public HashMap<Integer,Monom> getExpresie() {
        return expresie;
    }

    public void addMonom(Monom m){
        if(this.expresie.containsKey(m.getExponent())){
            double coeficientNou = m.getCoeficient() + this.expresie.get(m.getExponent()).getCoeficient();
            this.expresie.get(m.getExponent()).setCoeficient(coeficientNou);
        }
        else this.expresie.put(m.getExponent(),m);
        if(this.grad<m.getExponent()) this.grad = m.getExponent();
    }
//metoda asta am scris-o ca sa combat null pointer exception-ul de la metoda suprascrisa toString
    public void completeTheMissingValuesWith0(){
        for(int i = 0; i<=this.grad;i++){
            if(!this.expresie.containsKey(i)){
                this.expresie.put(i,new Monom(0.0,i));
            }
        }
    }

    public void gradUpdate(){
        if(this.expresie.get(this.grad).getCoeficient()==0){
            boolean changed = false;
            completeTheMissingValuesWith0();
            for(int i = 1; i<=this.grad;i++){
                if(this.expresie.get(grad-i).getCoeficient()!=0){
                    this.grad-=i;
                    changed = true;
                    break;
                }
            }
            if(!changed) this.grad = 0;
        }
    }

    @Override
    public String toString(){
        StringBuilder str;
        //ne asiguram ca nu avem null pointeri in hashmap-ul polinomului
        this.completeTheMissingValuesWith0();
        //verificam daca gradul polinomului este mai mare decat 1 deoarece in cazul in care gradul este 1 sau 0, nu ar
        //mai fi nevoie de executarea blocului de cod de mai jos
        if (this.grad > 1) {
            //verificam daca coeficientul termenului cu cea mai mare putere este intreg sau nu iar dupa incepem sa construim
            //string-ul
            if(this.expresie.get(this.grad).getCoeficient()-Math.round(this.expresie.get(this.grad).getCoeficient())==0){
                //daca coeficientul este egal cu 1 nu are rost sa il mai adaugam in string
                if(this.expresie.get(this.grad).getCoeficient()==1.0){
                    str = new StringBuilder("x^"+this.grad);
                }
                else {
                    str = new StringBuilder(Math.round(this.expresie.get(this.grad).getCoeficient()) + "*x^" + this.grad);
                }
            }else{
                String coefString = String.format("%.2f",Math.abs(this.expresie.get(this.grad).getCoeficient()));
                str = new StringBuilder(coefString + "*x^"+this.grad);
            }
            //parcurgem restul termeniilor
            for(int i = 1; i<this.grad-1; i++){
                //verificam daca coeficientul este diferit de 0, in caz contrar nu va mai fi nimic adaugat la sir
                if(this.expresie.get(this.grad-i).getCoeficient()!=0.0){
                    //verificam daca termenul are coeficient pozitiv sau negativ pentru a stabili noul semn din operatie
                    //pe care il punem in string
                    if(this.expresie.get(this.grad-i).getCoeficient()<0) str.append(" - ");
                    else str.append(" + ");
                    //aici se face exact ce s-a facut la monomul cu exponentul cel mai ridicat
                    if(this.expresie.get(this.grad-i).getCoeficient()-Math.round(this.expresie.get(this.grad-i).getCoeficient())==0){
                        if(this.expresie.get(this.grad-i).getCoeficient()==1.0){
                            str.append("x^").append(this.expresie.get(this.grad-i).getExponent());
                        }
                        else{
                            str.append(Math.round(Math.abs(this.expresie.get(this.grad-i).getCoeficient()))).append("*x^").append(this.expresie.get(this.grad-i).getExponent());
                        }
                    }
                    else{
                        String coefString = String.format("%.2f",Math.abs(this.expresie.get(this.grad-i).getCoeficient()));
                        str.append(coefString).append("*x^").append(this.expresie.get(this.grad-i).getExponent());
                    }
                }
            }
            //adaugarea la string ca monomului de grad 1
            if(this.expresie.get(1).getCoeficient()!=0.0){
                if(this.expresie.get(1).getCoeficient()<0) str.append(" - ");
                else str.append(" + ");
                if(this.expresie.get(1).getCoeficient()-Math.round(this.expresie.get(1).getCoeficient())==0){
                    if(this.expresie.get(1).getCoeficient()==1){
                        str.append("x");
                    }
                    else {
                        str.append(Math.round(Math.abs(this.expresie.get(1).getCoeficient()))).append("*x");
                    }
                }
                else {
                    String coefString = String.format("%.2f",Math.abs(this.expresie.get(1).getCoeficient()));
                    str.append(coefString).append("*x");
                }
            }
            //adaugarea la string ca monomului de grad 0
            if(this.expresie.get(0).getCoeficient()!=0.0){
                if(this.expresie.get(0).getCoeficient()<0) str.append(" - ");
                else str.append(" + ");
                if(this.expresie.get(0).getCoeficient()-Math.round(this.expresie.get(0).getCoeficient())==0){
                    str.append(Math.round(Math.abs(this.expresie.get(0).getCoeficient())));
                }
                else {
                    String coefString = String.format("%.2f",Math.abs(Math.abs(this.expresie.get(0).getCoeficient())));
                    str.append(coefString);
                }
            }
        }
        else if(this.grad==1){
            if(this.expresie.get(1).getCoeficient()-Math.round(this.expresie.get(1).getCoeficient())==0){
                if(this.expresie.get(1).getCoeficient()==1){
                    str = new StringBuilder("x");
                }
                else if(this.expresie.get(1).getCoeficient()==-1){
                    str = new StringBuilder("-x");
                }
                else str = new StringBuilder(Math.round(this.expresie.get(this.grad).getCoeficient()) + "*x");
            }else{
                String coefString = String.format("%.2f",Math.abs(this.expresie.get(1).getCoeficient()));
                str = new StringBuilder(coefString + "*x");
            }
            if(this.expresie.get(0).getCoeficient()!=0.0){
                if(this.expresie.get(0).getCoeficient()<0) str.append(" - ");
                else str.append(" + ");
                if(this.expresie.get(0).getCoeficient()-Math.round(this.expresie.get(0).getCoeficient())==0){
                    str.append(Math.round(Math.abs(this.expresie.get(0).getCoeficient())));
                }
                else {
                    String coefString = String.format("%.2f",Math.abs(Math.abs(this.expresie.get(0).getCoeficient())));
                    str.append(coefString);
                }
            }
        }
        else{
            if(this.expresie.get(0).getCoeficient()-Math.round(this.expresie.get(0).getCoeficient())==0){
                str = new StringBuilder(String.valueOf(Math.round(Math.abs(this.expresie.get(0).getCoeficient()))));
            }
            else {
                String coefString = String.format("%.2f",Math.abs(Math.abs(this.expresie.get(0).getCoeficient())));
                str = new StringBuilder(coefString);
            }
        }
        return str.toString();
    }
}
