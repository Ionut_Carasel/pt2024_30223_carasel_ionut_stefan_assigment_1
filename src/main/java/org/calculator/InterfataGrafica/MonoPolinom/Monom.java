package main.java.org.calculator.InterfataGrafica.MonoPolinom;

public class Monom {
    private double coeficient;
    private int exponent;

    public Monom(double coeficient) {
        this.coeficient = coeficient;
    }

    public Monom(double coeficient, int exponent) {
        this.coeficient = coeficient;
        if(coeficient==0.0) this.exponent = 0;
        else this.exponent = exponent;
    }
    public Monom(int exponent) {
        this.exponent = exponent;
    }
    public double getCoeficient() {
        return coeficient;
    }

    public void setCoeficient(double coeficient) {
        this.coeficient = coeficient;
    }

    public int getExponent() {
        return exponent;
    }

}
