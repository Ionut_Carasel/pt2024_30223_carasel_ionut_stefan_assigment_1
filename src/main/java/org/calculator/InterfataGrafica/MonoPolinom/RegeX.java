package main.java.org.calculator.InterfataGrafica.MonoPolinom;
import java.util.regex.*;
import java.util.ArrayList;
public class RegeX {
    Pattern standardString1 = Pattern.compile("([+|-]?)(\\d+)\\*?x?\\^?(\\d*)");
    //Pattern standardString2 = Pattern.compile("([+|-]?)?x\\^?(\\d*)");
    String polinomString;

    public RegeX(String polinomString) {
            this.polinomString = polinomString;
    }

    public Polinom generatePolinom(Polinom p){
        Matcher match1 = standardString1.matcher(polinomString);
        ArrayList<String> poli1 = new ArrayList<>();
        while (match1.find()){
            poli1.add(match1.group());
        }
        for(String s:poli1){
            double coeficient;
            int exponent;
            if(s.contains("*x^")){
                String[] parsare = s.split("\\*x\\^");
                coeficient = Double.parseDouble(parsare[0]);
                exponent = Integer.parseInt(parsare[1]);
                Monom m = new Monom(coeficient,exponent);
                p.addMonom(m);
            }
            else if(s.contains("x^")){
                String[] parsare = s.split("x\\^");
                coeficient = Double.parseDouble(parsare[0]);
                exponent = Integer.parseInt(parsare[1]);
                Monom m = new Monom(coeficient,exponent);
                p.addMonom(m);
            }
            else if(s.contains("*x")){
                String[] parsare = s.split("\\*x");
                coeficient = Double.parseDouble(parsare[0]);
                Monom m = new Monom(coeficient,1);
                p.addMonom(m);
            }
            else if(s.contains("x")){
                String[] parsare = s.split("x");
                coeficient = Double.parseDouble(parsare[0]);
                Monom m = new Monom(coeficient,1);
                p.addMonom(m);
            }
            else{
                coeficient = Double.parseDouble(s);
                Monom m = new Monom(coeficient,0);
                p.addMonom(m);
            }
        }

        return p;
    }
}